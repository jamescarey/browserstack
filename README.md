# BrowserStack
---

A project for quickly provisioning virtual machines with different browsers

## Options

| ```./stack``` | shows command options (if missing will download any mandatory images ~1.4Gb)           |
| ```./stack init -m chrome-stable -i "192.168.2.5" -p 3395``` | provision chrome-stable browser         |
| ```./stack up -m chrome-stable -i "192.168.2.5"```           | up chrome-stable browser                |
| ```./stack connect -m chrome-stable```                       | connect to the chrome-stable browser vm |
| ```./stack down -m chrome-stable```                          | halt the chrome-stable browser vm       |
