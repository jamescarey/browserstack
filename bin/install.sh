#!/bin/bash

em='tput smso'
off='tput rmso'

dpkg-query -s virtualbox-4.2 > /dev/null
if [ $? -ne 0 ]; then
  ${em}; echo "Unable to find Virtual Box install, INSTALLING Virtual Box (4.2.10)"; ${off};
  wget http://download.virtualbox.org/virtualbox/4.2.10/virtualbox-4.2_4.2.10-84104~Ubuntu~precise_i386.deb
  sudo dpkg -i ./virtualbox-4.2_4.2.10-84104~Ubuntu~precise_i386.deb
  rm ./virtualbox-4.2_4.2.10-84104~Ubuntu~precise_i386.deb
  wget http://download.virtualbox.org/virtualbox/4.2.10/Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
  sudo VBoxManage extpack install ./Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
  rm ./Oracle_VM_VirtualBox_Extension_Pack-4.2.10-84104.vbox-extpack
fi

dpkg-query -s vagrant > /dev/null
if [ $? -ne 0 ]; then
  ${em}; echo 'Unable to find Vagrant install, INSTALLING Vagrant (1.2.2)'; ${off};
  wget http://files.vagrantup.com/packages/7e400d00a3c5a0fdf2809c8b5001a035415a607b/vagrant_1.2.2_i686.deb
  sudo dpkg -i ./vagrant_1.2.2_i686.deb
  rm ./vagrant_1.2.2_i686.deb
  vagrant plugin install vagrant-list
fi

if [ ! -d "$HOME/.rbenv" ]; then
  #http://dan.carley.co/blog/2012/02/07/rbenv-and-bundler/
  git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile
  echo 'eval "$(rbenv init -)"' >> ~/.profile
  exec $SHELL -l

  sudo apt-get install build-essential
  sudo apt-get install libreadline-dev libssl-dev zlib1g-dev
  sudo apt-get install autoconf bison libxml2-dev

  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  rbenv install 2.0.0-dev
  rbenv local 2.0.0-dev
  gem update --system

  git clone git://github.com/carsomyr/rbenv-bundler.git ~/.rbenv/plugins/bundler

  gem install bundler

  mkdir ~/.bundle
  touch ~/.bundle/config
  echo "---" >> ~/.bundle/config
  echo "BUNDLE_PATH: vendor" >> ~/.bundle/config
  echo 'BUNDLE_DISABLE_SHARED_GEMS: "1"' >> ~/.bundle/config
  rbenv rehash
fi

#if [ ! -d "./lib/" ]; then
#  ${em}; echo 'Cloning windows-fromscratch library'; ${off};
#  mkdir ./lib/
#  cd lib
#  git clone git://github.com/hh/windows-fromscratch.git
#  cd windows-fromscratch
#  rbenv local 2.0.0-dev
#  bundle install
#  bundle exec bash
#  rbenv rehash
#  cd ../../
#fi
